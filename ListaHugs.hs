-- Questão 1
sameFour :: (Eq x) => [x] -> Bool 
sameFour [a,b,c,d] =
    if (a == b && a == c && a == d) 
        then True
        else False

-- Questão 2
percorreSegundaLista :: Eq x => x -> [x] -> [x]
percorreSegundaLista a [] = []
percorreSegundaLista a (b:bs)
    | a == b = [a]
    | otherwise = percorreSegundaLista a bs

removeRedundancia :: Eq x => [x] -> [x]
removeRedundancia [] = []
removeRedundancia (a:as)
    | [a] /= (percorreSegundaLista a as) = [a] ++ removeRedundancia as
    | otherwise = [] ++ removeRedundancia as

(^.^) :: Eq x => [x] -> [x] -> [x]
[] ^.^ _ = []
_ ^.^ [] = []
(a:as) ^.^ b = removeRedundancia $ percorreSegundaLista a b ++ (as ^.^ b)
