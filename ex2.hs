import Data.List

(<|>) :: (Eq x) => [x] -> [x] -> [x]

(<|>) list1 list2 = list1 `intersect` list2